package pl.better.solutions.model;

public enum ShareActions {

    WIG("WIG", "aq_wig_c2"),
    WIG20("WIG20", "aq_wig20_c2"),
    WIG20_FUT("WIG20 Fut", "aq_fw20_c0"),
    M_WIG40("mWIG40", "aq_mwig40_c2"),
    S_WIG80("sWIG80", "aq_swig80_c2");

    private final String name;
    private final String id;

    ShareActions(String name, String id) {

        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}

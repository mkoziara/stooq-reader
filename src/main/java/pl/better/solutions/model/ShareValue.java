package pl.better.solutions.model;

import java.util.Objects;

public class ShareValue {

    private final String shareName;
    private final double shareValue;

    public ShareValue(String shareName, double shareValue) {
        this.shareName = shareName;
        this.shareValue = shareValue;
    }

    public String getShareName() {
        return shareName;
    }

    public double getShareValue() {
        return shareValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShareValue that = (ShareValue) o;
        return Double.compare(that.shareValue, shareValue) == 0 &&
                Objects.equals(shareName, that.shareName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shareName, shareValue);
    }
}

package pl.better.solutions.model.entry;

import pl.better.solutions.csv.CsvEntry;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CsvShareEntry implements CsvEntry {

    private static final String TIMESTAMP = "timestamp";

    private final Map<String, Double> entryData;
    private final LocalTime timestamp;


    public CsvShareEntry(Map<String, Double> entryData, LocalTime timestamp) {
        this.entryData = entryData;
        this.timestamp = timestamp;
    }

    @Override
    public String[] getHeaders() {
        Set<String> keys = new HashSet<>(entryData.keySet());
        keys.add(TIMESTAMP);
        String[] headers = new String[keys.size()];

        return keys.toArray(headers);
    }

    @Override
    public Map<String, Object> getEntryData() {
        Map<String, Object> data = new HashMap<>(entryData);
        data.put(TIMESTAMP, timestamp.format(DateTimeFormatter.ISO_LOCAL_TIME));

        return data;
    }
}

package pl.better.solutions.model.entry;

import pl.better.solutions.model.ShareValue;

import javax.inject.Singleton;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ShareEntryFactory {

    private static final Double EMPTY_VALUE = null;
    private Map<String, Double> lastValues = new HashMap<>();

    public CsvShareEntry createShareEntry(List<ShareValue> shareValues) {

        Map<String, Double> entryData = shareValues.stream()
                .collect(Collectors.toMap(ShareValue::getShareName, ShareValue::getShareValue));

        Map<String, Double> updatedData = removeSameValues(entryData);
        return new CsvShareEntry(updatedData, LocalTime.now());
    }

    private Map<String, Double> removeSameValues(Map<String, Double> entryData) {

        Map<String, Double> dataBuffer = new HashMap<>(entryData);

        lastValues.keySet()
                .stream()
                .filter(s -> Objects.nonNull(entryData.get(s)))
                .filter(s -> isSameAsBefore(s, entryData.get(s)))
                .forEach(s -> entryData.put(s, EMPTY_VALUE));

        lastValues = dataBuffer;
        return entryData;
    }

    private boolean isSameAsBefore(String key, double value) {
        return lastValues.get(key).equals(value);
    }
}

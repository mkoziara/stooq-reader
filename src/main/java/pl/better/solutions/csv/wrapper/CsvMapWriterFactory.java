package pl.better.solutions.csv.wrapper;

import com.google.inject.Inject;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.prefs.CsvPreference;
import pl.better.solutions.cmd.CommandLineArguments;
import pl.better.solutions.csv.exception.CsvFileException;

import javax.inject.Singleton;
import java.io.FileWriter;
import java.io.IOException;

@Singleton
public class CsvMapWriterFactory {

    private final CommandLineArguments arguments;

    @Inject
    public CsvMapWriterFactory(CommandLineArguments arguments) {
        this.arguments = arguments;
    }

    public CsvMapWriterWrapper createWriter() {
        FileWriter writer = createFileWriter();
        CsvMapWriter csvMapWriter = new CsvMapWriter(writer, CsvPreference.STANDARD_PREFERENCE);

        return new CsvMapWriterWrapper(csvMapWriter);
    }

    private FileWriter createFileWriter() {
        try {
            return new FileWriter(arguments.getTargetFile(), true);
        } catch (IOException e) {
            throw new CsvFileException(e);
        }
    }
}
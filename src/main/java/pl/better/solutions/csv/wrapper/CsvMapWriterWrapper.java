package pl.better.solutions.csv.wrapper;

import org.supercsv.io.CsvMapWriter;
import pl.better.solutions.csv.CsvEntry;
import pl.better.solutions.csv.exception.CsvFileException;

import java.io.IOException;
import java.util.Map;

public class CsvMapWriterWrapper {

    private final CsvMapWriter csvMapWriter;

    CsvMapWriterWrapper(CsvMapWriter csvMapWriter) {
        this.csvMapWriter = csvMapWriter;
    }

    public void writeEntryWithHeader(CsvEntry csvShareEntry) {
        String[] headers = csvShareEntry.getHeaders();
        writeHeaders(headers);
        writeEntry(csvShareEntry);
    }

    public void writeEntry(CsvEntry csvShareEntry) {
        String[] headers = csvShareEntry.getHeaders();
        writeEntry(csvShareEntry.getEntryData(), headers);
    }

    private void writeHeaders(String[] headers) {
        try {
            csvMapWriter.writeHeader(headers);
        } catch (IOException e) {
            throw new CsvFileException(e);
        }
    }

    private void writeEntry(Map<String, Object> data, String[] headers) {
        try {
            csvMapWriter.write(data, headers);
            csvMapWriter.close();
        } catch (IOException e) {
            throw new CsvFileException(e);
        }
    }
}
package pl.better.solutions.csv;

import com.google.inject.Inject;
import pl.better.solutions.cmd.CommandLineArguments;
import pl.better.solutions.csv.wrapper.CsvMapWriterWrapper;
import pl.better.solutions.csv.wrapper.CsvMapWriterFactory;

import javax.inject.Singleton;

@Singleton
class CsvEntryWriter {

    private final CsvMapWriterFactory writerFactory;
    private final CommandLineArguments arguments;

    @Inject
    public CsvEntryWriter(CsvMapWriterFactory writerFactory, CommandLineArguments arguments) {
        this.writerFactory = writerFactory;
        this.arguments = arguments;
    }

    void writeToCsv(CsvEntry shareEntry) {
        CsvMapWriterWrapper writer = writerFactory.createWriter();

        if(hasLines()) {
            writer.writeEntry(shareEntry);
        } else {
            writer.writeEntryWithHeader(shareEntry);
        }
    }

    private boolean hasLines() {
        return arguments.getTargetFile().length() != 0;
    }
}

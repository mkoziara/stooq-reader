package pl.better.solutions.csv.exception;

public class CsvFileException extends RuntimeException {

    public CsvFileException(Throwable cause) {
        super(cause);
    }
}

package pl.better.solutions.csv;

import java.util.Map;

public interface CsvEntry {

    String[] getHeaders();

    Map<String, Object> getEntryData();

}

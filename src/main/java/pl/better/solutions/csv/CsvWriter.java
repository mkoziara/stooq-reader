package pl.better.solutions.csv;

import com.google.inject.Inject;
import pl.better.solutions.model.ShareValue;
import pl.better.solutions.model.entry.CsvShareEntry;
import pl.better.solutions.model.entry.ShareEntryFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class CsvWriter {

    private final ShareEntryFactory shareEntryFactory;
    private final CsvEntryWriter csvEntryWriter;

    @Inject
    public CsvWriter(ShareEntryFactory shareEntryFactory, CsvEntryWriter csvEntryWriter) {
        this.shareEntryFactory = shareEntryFactory;
        this.csvEntryWriter = csvEntryWriter;
    }

    public void writeToCsv(List<ShareValue> shareValues) {
        CsvShareEntry shareEntry = shareEntryFactory.createShareEntry(shareValues);
        csvEntryWriter.writeToCsv(shareEntry);
    }
}
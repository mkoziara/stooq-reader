package pl.better.solutions.guice;

import com.google.inject.AbstractModule;
import pl.better.solutions.cmd.CommandLineArguments;

public class GuiceModule extends AbstractModule {

    private final CommandLineArguments commandLineArguments;

    public GuiceModule(CommandLineArguments commandLineArguments) {
        this.commandLineArguments = commandLineArguments;
    }

    protected void configure() {
        bind(CommandLineArguments.class).toInstance(commandLineArguments);
    }
}

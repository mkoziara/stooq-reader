package pl.better.solutions;

import javax.inject.Singleton;
import java.util.Scanner;

@Singleton
class StooqReaderAppCloser {

    void waitForUserInput() {
        Scanner scanner = new Scanner(System.in);
        scanner.next();
        scanner.close();
        System.exit(0);
    }
}

package pl.better.solutions;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.better.solutions.client.StooqClient;
import pl.better.solutions.cmd.CommandLineArguments;
import pl.better.solutions.csv.CsvWriter;
import pl.better.solutions.parser.HtmlParser;
import rx.Observable;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Singleton
public class StooqReaderApp {

    private final StooqClient stooqClient;
    private final HtmlParser htmlParser;
    private final CsvWriter csvWriter;
    private final CommandLineArguments arguments;

    @Inject
    public StooqReaderApp(StooqClient stooqClient, HtmlParser htmlParser, CsvWriter csvWriter, CommandLineArguments arguments) {
        this.stooqClient = stooqClient;
        this.htmlParser = htmlParser;
        this.csvWriter = csvWriter;
        this.arguments = arguments;
    }

    public void run() {
        Observable.interval(arguments.getFrequency(), TimeUnit.MILLISECONDS)
                .flatMap(i -> stooqClient.getMainPageHtml())
                .map(htmlParser::parseHtml)
                .subscribe(csvWriter::writeToCsv, System.err::print);
    }
}
package pl.better.solutions.client;


import com.google.inject.Inject;
import rx.Observable;

import javax.inject.Singleton;

@Singleton
public class StooqClient {

    private static final String STOOQ_MAIN_PAGE = "http://www.stooq.pl";
    private final RxClientWrapper rxClient;

    @Inject
    public StooqClient(RxClientWrapper rxClient) {
        this.rxClient = rxClient;
    }

    public Observable<String> getMainPageHtml() {
        return rxClient.get(STOOQ_MAIN_PAGE)
                .map(response -> response.readEntity(String.class));
    }
}

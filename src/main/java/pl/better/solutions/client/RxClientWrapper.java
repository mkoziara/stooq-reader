package pl.better.solutions.client;

import com.google.inject.Singleton;
import org.glassfish.jersey.client.rx.RxClient;
import org.glassfish.jersey.client.rx.rxjava.RxObservable;
import org.glassfish.jersey.client.rx.rxjava.RxObservableInvoker;
import rx.Observable;

import javax.ws.rs.core.Response;

@Singleton
public class RxClientWrapper {

    private final RxClient<RxObservableInvoker> rxClient;

    public RxClientWrapper() {
        this.rxClient = RxObservable.newClient();
    }

    public Observable<Response> get(String targetUri) {

        return rxClient.target(targetUri)
                .request()
                .rx()
                .get();
    }
}

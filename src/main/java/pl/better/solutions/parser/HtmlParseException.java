package pl.better.solutions.parser;

import pl.better.solutions.model.ShareActions;

public class HtmlParseException extends RuntimeException {

    private static final String ERROR_MESSAGE = "Unable to find <div> with id %s";

    public HtmlParseException(ShareActions action) {
        super(String.format(ERROR_MESSAGE, action.getId()));
    }
}

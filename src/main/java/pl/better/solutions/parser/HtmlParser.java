package pl.better.solutions.parser;

import pl.better.solutions.model.ShareActions;
import pl.better.solutions.model.ShareValue;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HtmlParser {

    private final String PATTERN_FORMAT = "<span id=%s>(.+?)</span>";

    public List<ShareValue> parseHtml(String html) {
        return Stream.of(ShareActions.values())
                .map(shareAction -> findShareValue(html, shareAction))
                .collect(Collectors.toList());
    }

    private ShareValue findShareValue(String html, ShareActions action) {
        Pattern compile = Pattern.compile(String.format(PATTERN_FORMAT, action.getId()));
        Matcher matcher = compile.matcher(html);

        if(matcher.find()) {
            String value = matcher.group(1);
            return new ShareValue(action.getName(), Double.parseDouble(value));
        } else {
            throw new HtmlParseException(action);
        }
    }
}

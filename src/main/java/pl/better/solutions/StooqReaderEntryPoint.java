package pl.better.solutions;

import com.google.inject.Guice;
import com.google.inject.Injector;
import pl.better.solutions.cmd.CommandLineArguments;
import pl.better.solutions.cmd.CommandLineParser;
import pl.better.solutions.guice.GuiceModule;

public class StooqReaderEntryPoint {

    public static void main(String[] args) {
        Injector injector = createInjector(args);
        injector.getInstance(StooqReaderApp.class).run();
        injector.getInstance(StooqReaderAppCloser.class).waitForUserInput();
    }

    private static Injector createInjector(String[] args) {
        CommandLineArguments arguments = new CommandLineParser().getCommandLineArguments(args);
        return Guice.createInjector(new GuiceModule(arguments));
    }
}

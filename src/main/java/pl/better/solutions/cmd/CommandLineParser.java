package pl.better.solutions.cmd;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class CommandLineParser {

    private static final String PARSE_ERROR_MESSAGE = "Can't parse imput arguments";

    public CommandLineArguments getCommandLineArguments(String[] args) {

        CommandLineArguments commandLineArguments = new CommandLineArguments();
        CmdLineParser cmdLineParser = new CmdLineParser(commandLineArguments);

        try {
            cmdLineParser.parseArgument(args);
            return commandLineArguments;
        } catch (CmdLineException e) {
            cmdLineParser.printUsage(System.err);
            throw new RuntimeException(PARSE_ERROR_MESSAGE, e);
        }
    }
}

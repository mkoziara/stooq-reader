package pl.better.solutions.cmd;

import org.kohsuke.args4j.Option;

import java.io.File;

public class CommandLineArguments {

    private final static long DEFAULT_FREQUENCY = 60000;

    @Option(name="--updateFrequency")
    private long frequency = DEFAULT_FREQUENCY;
    @Option(name = "--targetFile", required = true)
    private File targetFile;

    public long getFrequency() {
        return frequency;
    }

    public File getTargetFile() {
        return targetFile;
    }
}

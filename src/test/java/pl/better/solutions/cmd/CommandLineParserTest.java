package pl.better.solutions.cmd;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class CommandLineParserTest {

    private final CommandLineParser testObj = new CommandLineParser();

    @Test
    public void shouldReturnUrlAndPath() throws Exception {
        // given
        Path expectedPath = Paths.get("test", "file");

        // when
        CommandLineArguments result = testObj.getCommandLineArguments(new String[]{
                "--updateFrequency", "500",
                "--targetFile", expectedPath.toString()
        });

        // then
        assertThat(result.getFrequency()).isEqualTo(500);
        assertThat(result.getTargetFile()).isEqualTo(expectedPath.toFile());
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowException_whenTargetFileDoesNotExist() throws Exception {
        // when
        testObj.getCommandLineArguments(new String[]{});
    }

    @Test()
    public void shouldReturnDefaultFrequencyValue_whenNotSet() throws Exception {
        // when
        CommandLineArguments result = testObj.getCommandLineArguments(new String[]{"--targetFile", "/test/file"});

        // then
        assertThat(result.getFrequency()).isEqualTo(60000);
    }
}
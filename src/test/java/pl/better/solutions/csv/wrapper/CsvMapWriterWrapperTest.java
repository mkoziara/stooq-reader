package pl.better.solutions.csv.wrapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.supercsv.io.CsvMapWriter;
import pl.better.solutions.csv.CsvEntry;
import pl.better.solutions.csv.exception.CsvFileException;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CsvMapWriterWrapperTest {

    @InjectMocks
    private CsvMapWriterWrapper testObj;
    @Mock
    private CsvMapWriter csvMapWriter;
    @Mock
    private CsvEntry givenEntry;

    private String[] givenHeaders = new String[]{"given", "headers"};
    private final Map<String, Object> givenEntryData = Collections.singletonMap("given", "data");

    @Before
    public void setUp() throws Exception {
        when(givenEntry.getHeaders()).thenReturn(givenHeaders);
        when(givenEntry.getEntryData()).thenReturn(givenEntryData);
    }

    @Test
    public void shouldWriteEntry() throws IOException {
        // given

        // when
        testObj.writeEntry(givenEntry);

        // then
        verify(csvMapWriter).write(givenEntryData , givenHeaders);
        verify(csvMapWriter).close();
    }

    @Test
    public void shouldWriteEntryWithHeaders() throws IOException {
        // given
        String[] givenHeaders = {"given", "headers"};
        when(givenEntry.getHeaders()).thenReturn(givenHeaders);

        Map<String, Object> givenEntryData = Collections.singletonMap("given", "data");
        when(givenEntry.getEntryData()).thenReturn(givenEntryData);

        // when
        testObj.writeEntryWithHeader(givenEntry);

        // then
        verify(csvMapWriter).writeHeader(givenHeaders);
        verify(csvMapWriter).write(givenEntryData, givenHeaders);
        verify(csvMapWriter).close();
    }

    @Test(expected = CsvFileException.class)
    public void shouldThrowCsvFileException_whenUnableToWriteHeaders() throws IOException {
        // given
        doThrow(IOException.class).when(csvMapWriter).writeHeader(givenHeaders);

        // when
        testObj.writeEntryWithHeader(givenEntry);
    }

    @Test(expected = CsvFileException.class)
    public void shouldThrowCsvFileException_whenUnableToWriteEntry() throws IOException {
        // given
        doThrow(IOException.class).when(csvMapWriter).write(givenEntryData, givenHeaders);

        // when
        testObj.writeEntry(givenEntry);
    }
}
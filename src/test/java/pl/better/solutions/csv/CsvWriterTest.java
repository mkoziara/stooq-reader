package pl.better.solutions.csv;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.better.solutions.model.ShareValue;
import pl.better.solutions.model.entry.CsvShareEntry;
import pl.better.solutions.model.entry.ShareEntryFactory;

import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CsvWriterTest {

    @InjectMocks
    private CsvWriter testObj;
    @Mock
    private ShareEntryFactory shareEntryFactory;
    @Mock
    private CsvEntryWriter csvEntryWriter;

    @Test
    public void shouldCreateCsvEntry_andWriteToCSV() {
        // given
        ShareValue givenShareValue = new ShareValue("shareName", 159);
        List<ShareValue> shareValues = Collections.singletonList(givenShareValue);

        CsvShareEntry expectedEntry = new CsvShareEntry(Collections.singletonMap("shareName", 159.0), LocalTime.MAX);
        when(shareEntryFactory.createShareEntry(shareValues)).thenReturn(expectedEntry);

        // when
        testObj.writeToCsv(shareValues);

        // then
        verify(csvEntryWriter).writeToCsv(expectedEntry);
    }
}
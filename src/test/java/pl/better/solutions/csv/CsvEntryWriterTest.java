package pl.better.solutions.csv;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.better.solutions.cmd.CommandLineArguments;
import pl.better.solutions.csv.wrapper.CsvMapWriterFactory;
import pl.better.solutions.csv.wrapper.CsvMapWriterWrapper;
import pl.better.solutions.model.entry.CsvShareEntry;

import java.time.LocalTime;
import java.util.Collections;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CsvEntryWriterTest {

    @InjectMocks
    private CsvEntryWriter testObj;
    @Mock
    private CsvMapWriterFactory csvMapWriterFactory;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private CommandLineArguments arguments;
    @Mock
    private CsvMapWriterWrapper csvMapWriter;

    @Test
    public void shouldWriteEntryWithoutHeader_whenFileIsNotEmpty() {
        // given
        when(csvMapWriterFactory.createWriter()).thenReturn(csvMapWriter);
        when(arguments.getTargetFile().length()).thenReturn(15L);

        CsvShareEntry givenEntry = new CsvShareEntry(Collections.emptyMap(), LocalTime.MAX);

        // when
        testObj.writeToCsv(givenEntry);

        // then
        verify(csvMapWriter).writeEntry(givenEntry);
        verify(csvMapWriter, never()).writeEntryWithHeader(givenEntry);
    }

    @Test
    public void shouldWriteEntryWithHeader_whenFileIsEmpty() {
        // given
        when(csvMapWriterFactory.createWriter()).thenReturn(csvMapWriter);
        when(arguments.getTargetFile().length()).thenReturn(0L);

        CsvShareEntry givenEntry = new CsvShareEntry(Collections.emptyMap(), LocalTime.MAX);

        // when
        testObj.writeToCsv(givenEntry);

        // then
        verify(csvMapWriter).writeEntryWithHeader(givenEntry);
        verify(csvMapWriter, never()).writeEntry(givenEntry);
    }
}
package pl.better.solutions.model.entry;

import org.junit.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CsvShareEntryTest {


    private final String timestampHeader = "timestamp";

    @Test
    public void shouldGetProperHeaders() {
        // given
        String givenShareName = "shareName";
        Map<String, Double> givenShareData = Collections.singletonMap(givenShareName, 56.7);
        LocalTime givenTimestamp = LocalTime.MAX;

        CsvShareEntry givenShareEntry = new CsvShareEntry(givenShareData, givenTimestamp);

        // when
        String[] result = givenShareEntry.getHeaders();

        // then
        assertThat(result).contains(givenShareName, timestampHeader);
    }

    @Test
    public void shouldGetProperEntryData() {
        // given
        String givenShareName = "shareName";
        double givenShareValue = 56.7;
        Map<String, Double> givenShareData = Collections.singletonMap(givenShareName, givenShareValue);

        LocalTime givenTimestamp = LocalTime.MAX;
        CsvShareEntry givenShareEntry = new CsvShareEntry(givenShareData, givenTimestamp);

        // when
        Map<String, Object> result = givenShareEntry.getEntryData();

        // then
        assertThat(result).containsEntry(givenShareName, givenShareValue);
        assertThat(result).containsEntry(timestampHeader, givenTimestamp.format(DateTimeFormatter.ISO_LOCAL_TIME));
    }
}
package pl.better.solutions.model.entry;

import org.junit.Test;
import pl.better.solutions.model.ShareValue;

import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ShareEntryFactoryTest {

    private ShareEntryFactory testObj = new ShareEntryFactory();

    @Test
    public void shouldCreateCsvShareEntry() {
        // given
        String givenShareName = "givenName";
        double givenValue = 159;
        ShareValue givenShareValue = new ShareValue(givenShareName, givenValue);

        // when
        CsvShareEntry result = testObj.createShareEntry(Collections.singletonList(givenShareValue));

        // then
        Map<String, Object> resultData = result.getEntryData();
        assertThat(resultData).hasSize(2);
        assertThat(resultData).containsEntry(givenShareName, givenValue);
        assertThat(resultData.get("timestamp")).isNotNull();
    }

    @Test
    public void shouldReplaceValueWithNull_whenLastTimeWasTheSameValue() {
        // given
        String givenShareName = "givenName";
        int givenValue = 159;
        ShareValue givenShareValue = new ShareValue(givenShareName, givenValue);
        testObj.createShareEntry(Collections.singletonList(givenShareValue));

        // when
        CsvShareEntry result = testObj.createShareEntry(Collections.singletonList(givenShareValue));

        // then
        Map<String, Object> resultData = result.getEntryData();
        assertThat(resultData).hasSize(2);
        assertThat(resultData.get(givenShareName)).isNull();
        assertThat(resultData.get("timestamp")).isNotNull();
    }
}
package pl.better.solutions.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rx.Observable;
import rx.observers.TestSubscriber;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StooqClientTest {

    @InjectMocks
    private StooqClient testObj;
    @Mock
    private RxClientWrapper rxClientWrapper;

    @Test
    public void shouldGetMainPageHtmlAsString() {
        // given
        String expectedHtml = "<html></html>";

        Response mock = mock(Response.class);
        when(mock.readEntity(String.class)).thenReturn(expectedHtml);

        when(rxClientWrapper.get("http://www.stooq.pl")).thenReturn(Observable.just(mock));

        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        // when
        testObj.getMainPageHtml()
                .subscribe(testSubscriber);

        // then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(expectedHtml);
    }
}
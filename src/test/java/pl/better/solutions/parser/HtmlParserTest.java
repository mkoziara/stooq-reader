package pl.better.solutions.parser;

import org.junit.Test;
import pl.better.solutions.model.ShareValue;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class HtmlParserTest {

    private HtmlParser testObj = new HtmlParser();

    @Test
    public void shouldFindAllShareValues() {
        // given
        String givenHtml = getHtml();

        // when
        List<ShareValue> result = testObj.parseHtml(givenHtml);

        // then
        assertThat(result).extracting(ShareValue::getShareName)
                .contains("WIG20", "WIG", "WIG20 Fut", "mWIG40", "sWIG80");

        assertThat(result).extracting(ShareValue::getShareValue)
                .contains(1.6, 2.9, 35d, 1596d, 8889d);

    }

    @Test(expected = HtmlParseException.class)
    public void shouldThrowHtmlParseException_whenUnableToFindSomeOfTheElements() {
        // given
        String givenHtml = "<html><span id=something/></html>";

        // when
        testObj.parseHtml(givenHtml);
    }

    private String getHtml() {
        return "<html>" +
                "<span id=aq_wig_c2>1.6</span>" +
                "<span id=aq_wig20_c2>2.9</span>" +
                "<span id=aq_mwig40_c2>35</span>" +
                "<span id=somethingelse>15</span>" +
                "<span id=aq_swig80_c2>1596</span>" +
                "<span id=aq_fw20_c0>8889</span>" +
                "</html>";
    }
}